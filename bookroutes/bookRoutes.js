const express = require('express')
const router = express.Router()

const { getAllbooks, getBookbyid, addBook, updateBook, deleteBook } = require("../bookController/bookController");

router.get('/',getAllbooks )
router.get('/:bookid',getBookbyid)
router.post('/',addBook)
router.patch('/:bookid',updateBook)
router.delete('/:bookid',deleteBook)


module.exports=router