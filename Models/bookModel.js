const mongoose = require('mongoose');
const { Schema } = mongoose;

const bookSchema = new Schema({
    image: String,
    title: String,
      price: Number,
      author:{
        type:mongoose.ObjectId,
        ref:"Author"
      },
  });

  const Book = mongoose.model('Book', bookSchema);
  module.exports= Book