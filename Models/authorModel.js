const mongoose = require('mongoose');
const { Schema } = mongoose;

const authorSchema = new Schema({
    name: String
  });

  const Author = mongoose.model('Author', authorSchema);
  module.exports=Author