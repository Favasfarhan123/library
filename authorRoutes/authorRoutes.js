const express = require('express')
const router = express.Router()

const { getAllauthors, getAuthorbyid, addAuthor, updateAuthor, deleteAuthor } = require("../authorController/authorController");

router.get('/',getAllauthors )
router.get('/:authorid',getAuthorbyid)
router.post('/',addAuthor)
router.patch('/:authorid',updateAuthor)
router.delete('/:authorid',deleteAuthor)


module.exports=router