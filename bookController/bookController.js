const express = require('express')
const app = express()
const port = 3000
const cors = require('cors')


const Book = require('../Models/bookModel');



const getAllbooks=async (req, res) => {
    const books = await Book.find({});
    res.status(200).json(books)
  }
const getBookbyid=async (req, res) => {
  try{
    const book=await Book.findById(req.params.bookid).exec();
    res.status(200).json(book)
  }
  catch{
    res.status(404).send('Book Not found')
  }
    
  }
const addBook=async (req, res) => {
  
    const book = new Book(req.body)
    await book.save();
    res.status(201).json(book)
  
  }
const updateBook=async (req, res) => {
  try{
    const updatedBook=await Book.findByIdAndUpdate(req.params.bookid,req.body,{new:true})
    res.status(200).json(updatedBook)
    await updatedBook.save();
  }
  catch{
    res.status(404).send('Book not found')
  }
  }
const deleteBook=async (req, res) => {
  try{
    const deletedBook=await Book.findByIdAndDelete(req.params.bookid)
    res.status(200).send("Category Deleted")

}
catch{
  res.status(404).send("Id not found")
}
  }

  module.exports={
    getAllbooks,
    getBookbyid,
    addBook,
    updateBook,
    updateBook,
    deleteBook
  }