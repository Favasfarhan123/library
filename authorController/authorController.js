
const express = require('express')
const app = express()
const port = 3000
const cors = require('cors')


const Author = require('../Models/authorModel');



const getAllauthors=async (req, res) => {
    const authors = await Author.find({});
    res.status(200).json(authors)
  }
const getAuthorbyid=async (req, res) => {
  try{
    const author=await Author.findById(req.params.authorid).exec();
    res.status(200).json(author)
  }
  catch{
    res.status(404).send('Author Not found')
  }
    
  }
const addAuthor=async (req, res) => {
  
    const author = new Author(req.body)
    await author.save();
    res.status(201).json(author)
  
  }
const updateAuthor=async (req, res) => {
  try{
    const updatedAuthor=await Author.findByIdAndUpdate(req.params.authorid,req.body,{new:true})
    res.status(200).json(updatedAuthor)
    await updatedAuthor.save();
  }
  catch{
    res.status(404).send('Author not found')
  }
  }
const deleteAuthor=async (req, res) => {
  try{
    const deletedAuthor=await Author.findByIdAndDelete(req.params.authorid)
    res.status(200).send("Category Deleted")

}
catch{
  res.status(404).send("Id not found")
}
  }

  module.exports={
    getAllauthors,
    getAuthorbyid,
    addAuthor,
    updateAuthor,
    updateAuthor,
    deleteAuthor
  }