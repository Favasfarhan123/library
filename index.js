
const express = require('express')
const app = express()
const port = 3000
const cors = require('cors')
const mongoose = require('mongoose');


const authors=require('./authorRoutes/authorRoutes')
const books=require('./bookroutes/bookRoutes')

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true }));
app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).send('Something broke!');
});


app.use('/authors',authors)
app.use('/books',books)

main().then(res => console.log('connected')).catch(err => console.log(err));
async function main() {
  await mongoose.connect('mongodb+srv://favasfarhanm:iHbNWj7qVKjjnZmf@library.tocnwjj.mongodb.net/?retryWrites=true&w=majority&appName=Library')
}

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})